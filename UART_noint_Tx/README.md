# UART_noint_Tx

This example provides a simple non-interrupt set of UART functions.
Printable ascii characters are sent at 9600 baud indefinitely at 1Hz.

The baud rate calculation assumes the CPU clock is 8MHz.

