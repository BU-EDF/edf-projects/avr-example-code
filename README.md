# AVR example code

Example code for "bare metal" programming on the Atmel (Microchip) family
of 8-bit microcontrollers.  It is assumed that the library [avr-libx](https://www.nongnu.org/avr-libc/) is used along with the avr-gcc compiler.

A sample Makefile is provided with each.  The code has been tested (only!) on ATMega328 chips unless otherwise noted.

| Code          | Description                                          |
|---------------|------------------------------------------------------|
| BlinkyExample | a minimal example which blinks the LED on an Arduino |
| UART_noint_Tx | a UART example which sends characters                |
| UART_stdio    | a UART example using standard I/O                    |


# Arduino example code

Here are a few Arduino examples.  These are useful when the Arduino
environment is preferred, or for boards like the `pro Micro` or
`Leonardo` with ATMega32u4 where bare metal download support is
difficult.

| Code                     | Description                                                   |
|--------------------------|---------------------------------------------------------------|
| SerialCommandExample.ino | command/response code which emulates gets() and parses tokens |
|                          |                                                               |
