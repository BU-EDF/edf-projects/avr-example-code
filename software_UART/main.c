/*
 * software UART example
 */

#include <stdio.h>
#include <avr/io.h>
#include <util/delay.h>

#include "sw_uart.h"

// Arduino LED is on PB5
#define LED_DDR DDRB
#define LED_BIT 5
#define LED_PORT PORTB

char buff[40];

int main (void)
{
  unsigned char c;

  LED_DDR |= (1 << LED_BIT);
  uart_init_tx();

  while( 1) {
    uart_puts("Test -->\n\r");
    c = uart_getc();
    snprintf( buff, sizeof(buff), "0x%02x '%c'\n\r", c, (c > 0x20 && c < 0x80) ? c : '?');
    uart_puts( buff);
  }
}


