//
// simple UART Tx/Rx (non-interrupt)
//
// uart_putc( ch) - send one character to UART
// uart_puts( s)  - send string to UART
// uart_getc()    - read one character from UART (waits for character)
//
// note that these functions depend on _delay_us() working properly,
// so you must be sure F_CPU is set correctly.  If you have interrupts
// running they will likely cause serial comms to be unreliable.
//
#include "sw_uart.h"
#include <util/delay.h>

// initialize
void uart_init_tx() {
  UART_DDR |= UART_TX_BIT;
}

// send a character to UART at 9600 baud using software delay
static void inline send_uart_bit( uint8_t b)
{
  if( b)
    UART_PORT |= UART_TX_BIT;
  else
    UART_PORT &= ~UART_TX_BIT;
  _delay_us( BAUD_DELAY_US);
}

// send byte to UART
void uart_putc( uint8_t c)
{
  send_uart_bit( 0);		/* start bit */
  send_uart_bit( c & 1);
  send_uart_bit( c & 2);
  send_uart_bit( c & 4);
  send_uart_bit( c & 8);
  send_uart_bit( c & 0x10);
  send_uart_bit( c & 0x20);
  send_uart_bit( c & 0x40);
  send_uart_bit( c & 0x80);
  send_uart_bit( 1);		/* stop bit */
}

// send string to UART
void uart_puts( char *s) {
  while( *s)
    uart_putc( *s++);
}


// receive a character
//   should really check for continued presence of start bit, stop bit etc
//   and report framing errors, but that would throw off the timing
//
inline unsigned char uart_getc() {
  unsigned char c = 0;
  while( UART_PIN & UART_RX_BIT) /* wait for start bit... high-to-low */
    ;
  _delay_us( BAUD_HALF_DELAY);	/* delay to middle of start bit */
  _delay_us( BAUD_RX_DELAY);	/* delay to middle of bit 0 */
  if( UART_PIN & UART_RX_BIT) c |= 1;
  _delay_us( BAUD_RX_DELAY);	/* delay to middle of bit 1... */
  if( UART_PIN & UART_RX_BIT) c |= 2;
  _delay_us( BAUD_RX_DELAY);
  if( UART_PIN & UART_RX_BIT) c |= 4;
  _delay_us( BAUD_RX_DELAY);
  if( UART_PIN & UART_RX_BIT) c |= 8;
  _delay_us( BAUD_RX_DELAY);
  if( UART_PIN & UART_RX_BIT) c |= 0x10;
  _delay_us( BAUD_RX_DELAY);
  if( UART_PIN & UART_RX_BIT) c |= 0x20;
  _delay_us( BAUD_RX_DELAY);
  if( UART_PIN & UART_RX_BIT) c |= 0x40;
  _delay_us( BAUD_RX_DELAY);
  if( UART_PIN & UART_RX_BIT) c |= 0x80;
  _delay_us( BAUD_RX_DELAY);
  _delay_us( BAUD_RX_DELAY);
  return c;
}
