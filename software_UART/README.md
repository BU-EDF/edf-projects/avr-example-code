# Software UART

This is a very simple software UART for the AVR family.  It is tested
and working (only) on an ATMega328 Arduino running at 16MHz.  It
*should* work on other AVRs at other speeds, but you may need to tweak
the timing in `sw_uart.h`.  See comments in that file.

    uart_putc( ch) - send one character to UART
    uart_puts( s)  - send string to UART
    uart_getc()    - read one character from UART (waits for character)

You will need to edit `sw_uart.h` to set your pins and ports.
Be sure your `Makefile` sets `F_CPU` to the CPU clock frequency.

Note that these functions depend on _delay_us() working properly,
so you must be sure F_CPU is set correctly.  If you have interrupts
running they will likely cause serial comms to be unreliable.

No gets() function is provided currently since it really should
include editing features (backspace, etc) and I'm lazy!
