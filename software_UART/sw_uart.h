//
// sw_uart.h - header file for simple software UART functions
//
#ifndef __SW_UART_TX_H
#define __SW_UART_TX_H

#include <avr/io.h>
#include <stdint.h>

// NOTE:  set these for your application
// Tx/Rx bits, for example Arduino 11 (Tx) and 10 (Rx)
#define UART_PORT PORTB
#define UART_PIN PINB
#define UART_DDR DDRB
#define UART_TX_BIT _BV(3)
#define UART_RX_BIT _BV(2)

// this works at 9600 baud on an arduino... may have to tweak the fudge value
// NOTE:  F_CPU must be correct else the delay functions may not work
#define BAUD_RATE 9600
// this is a fudge factor for function overhead correction,.
// if your F_CPU is not 16MHz you may have to tweak this.
#define BAUD_DELAY_FUDGE 3
// for transmist
#define BAUD_DELAY_US ((1000000/BAUD_RATE)-BAUD_DELAY_FUDGE)

// for receive
#define BAUD_RX_DELAY BAUD_DELAY_US
#define BAUD_HALF_DELAY (BAUD_DELAY_US/2)

void uart_init_tx();
void uart_putc( uint8_t c);
void uart_puts( char *s);
unsigned char uart_getc();

#endif
