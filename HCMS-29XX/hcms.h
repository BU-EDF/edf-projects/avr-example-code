//
// basic functions for HCMS-29xx display (4 digit version only)
//

// connections to the display (from Dani's test board)
#define LED_PORT PORTB
#define LED_DDR DDRB

#define LED_DIN_BIT 4
#define LED_RS_BIT 3
#define LED_CLK_BIT 2
#define LED_nCE_BIT 1
#define LED_nRST_BIT 0

#define LED_MASK_ALL (_BV(LED_DIN_BIT) |_BV(LED_RS_BIT) |_BV(LED_CLK_BIT) |_BV(LED_nCE_BIT) |_BV(LED_nRST_BIT))

void hcms_init();
void hcms_select_control();
void hcms_select_dot();
void hcms_shiftout( uint8_t val, uint8_t count, uint8_t ce);
void hcms_write_string( char *s);
void hcms_write_bitmap( uint8_t *b);
