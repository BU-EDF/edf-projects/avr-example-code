/*
 * HCMS-29xx display test
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <avr/io.h>
#include <util/delay.h>

#include "../libs/ioport.h"
#include "../libs/shiftout.h"

#include "hcms.h"

// define parameters of a bitmap
#define WID 20
#define HGT 7
static uint8_t dpy[20];

// macros to set and reset a pixel
#define set(x,y) (dpy[x%WID]|=(1<<(y%HGT)))
#define clr(x,y) (dpy[x%WID]&=~(1<<(y%HGT)))

// arry of points with direction and offset
#define MAXP 10
typedef struct {
  int8_t x, y;
  int8_t dx, dy;
  int8_t t;
} a_point;

static a_point p[MAXP];
static uint8_t np;		// number of active points


int main (void)
{
  hcms_init();			// initialize the display

  np = 0;			// start with zero points

  while(1) {

    // increment number of points, set to random position
    // and direction
    if( np < MAXP) {
      p[np].x = rand() % WID;
      p[np].y = rand() % HGT;
      p[np].dx = 2*(rand()&1)-1;
      p[np].dy = 2*(rand()&1)-1;
      ++np;
    } else {
      // reach the maximum, reset and display a message
      np = 0;
      memset( dpy, 0, sizeof(dpy));
      hcms_write_string("BOOM");
      _delay_ms( 500);
    }

    // update the points for a while
    for( uint8_t k=0; k<33; k++) {

      hcms_write_bitmap( dpy);

      // loop over the points
      for( uint8_t i=0; i<np; i++) {
	clr( p[i].x, p[i].y);  // clear old pixel location

	p[i].x += p[i].dx;     // increment coord and check for bounce
	if( p[i].x == WID) {
	  p[i].x = WID-1;
	  p[i].dx = -p[i].dx;
	}
	if( p[i].x < 0) {
	  p[i].x = 0;
	  p[i].dx = -p[i].dx;
	}

	p[i].y += p[i].dy;
	if( p[i].y == HGT) {
	  p[i].y = HGT-1;
	  p[i].dy = -p[i].dy;
	}
	if( p[i].y < 0) {
	  p[i].y = 0;
	  p[i].dy = -p[i].dy;
	}

	set(p[i].x,p[i].y);   // set new pixel location

      }
      // slightly smart delay so it doesn't get a lot slower
      // as the number of points increases
      _delay_ms( 20);
      for( uint8_t z=0; z<(MAXP-np); z++)
	_delay_us( 1200);

    }    

  }
}


