//
// HCMS-29XX control functions
//
// hcms_init()             initialize the disply using default settings
// hcms_write_string( s)   write 4 characters to the display
//

#include <avr/io.h>

#include "hcms.h"
#include "font5x7.h"

#include "../libs/ioport.h"
#include "../libs/shiftout.h"

// some handy macros
#define CLK_HI() set_io_bits(&LED_PORT,_BV(LED_CLK_BIT))
#define CLK_LO() clear_io_bits(&LED_PORT,_BV(LED_CLK_BIT))

#define RS_HI() set_io_bits(&LED_PORT,_BV(LED_RS_BIT))
#define RS_LO() clear_io_bits(&LED_PORT,_BV(LED_RS_BIT))

#define nCE_HI() set_io_bits(&LED_PORT,_BV(LED_nCE_BIT))
#define nCE_LO() clear_io_bits(&LED_PORT,_BV(LED_nCE_BIT))

#define DIN_HI() set_io_bits(&LED_PORT,_BV(LED_DIN_BIT))
#define DIN_LO() clear_io_bits(&LED_PORT,_BV(LED_DIN_BIT))

// NOTE:  the code below could be tightened up noticeably
// if flash space is tight.  For example the first 3 calls
// could be replaced with
//    set_io_bits( &LED_PORT, _BV(LED_CLK_BIT)|_BV(LED_nCE_BIT)|_BV(LED_RS_BIT))
// but it's harder to read

// select the control register.
void hcms_select_control() {
  CLK_HI();
  nCE_HI();
  RS_HI();
  nCE_LO();
  CLK_LO();
}

// select the dot register
void hcms_select_dot() {
  CLK_HI();
  nCE_HI();
  RS_LO();
  nCE_LO();
  CLK_LO();
}

// shift up to 8 bits from MSB, specifying state of nCE at end
// this is complicated because the datasheet says that when clock
// is high during the last bit shift-out that nCE must be taken high
// before the clock goes low.
void hcms_shiftout( uint8_t val, uint8_t count, uint8_t ce) {

  // if nCE=L then we can just do the shift
  if( !ce) {
    shiftOutMsb( &LED_PORT, LED_CLK_BIT, LED_DIN_BIT, val, count);
  } else {
    // we want nCE=H at the end.
    // first do all but the last leaving nCE as it is
    if( count > 1)
      shiftOutMsb( &LED_PORT, LED_CLK_BIT, LED_DIN_BIT, val, count-1);
    // now do the last bit "by hand" and set nCE=H in the middle
    // first set the dataf
    if( val & (1<<(count-1)))
      DIN_HI();
    else
      DIN_LO();
    CLK_HI();
    nCE_HI();
    CLK_LO();
  }
}


// initialize the display
void hcms_init()
{
  LED_DDR |= LED_MASK_ALL;	/* all are outputs */
  
  clear_io_bits( &LED_PORT, LED_MASK_ALL); /* set all bits low */
  set_io_bits( &LED_PORT, _BV( LED_nRST_BIT) | _BV( LED_nCE_BIT) ); /* nRST, nCE high */

  // set the control registers

  // control register 0
  // bit 7 -    always 0
  // bit 6 -    always 1  (0 to enable sleep mode which we never use)
  // bits 4,5 - '00' for default current ('11' for max current)
  // bits 3-0 - relative brightness 0-15.  I use '1111' which is maximum

  hcms_select_control();
  hcms_shiftout( 0x4f, 8, 1);

  // control register 1 is unimportant but included here for completeness
  // bit 7 -      always 1
  // all other bits are essentiall don't care and should be zeroes
  hcms_select_control();
  hcms_shiftout( 0x80, 8, 1);
}


// display a bitmap (20 bytes)
// bit 0 of first byte is upper left
void hcms_write_bitmap( uint8_t *b)
{
  hcms_select_dot();
  for( uint8_t i=0; i<20; i++) {      /* loop over 5 columns */
    if( i == 19)
      hcms_shiftout( *b++, 8, 1);
    else
      hcms_shiftout( *b++, 8, 0);
  }
}

// display a 4-character ascii string.
// Results are only guaranteed for ascii codes 0x20-0x7f
void hcms_write_string( char *str)
{
  hcms_select_dot();		/* switch to the dot register */

  for( uint8_t c=0; c<4; c++) {	       /* loop over 4 characters */
    for( uint8_t i=0; i<5; i++) {      /* loop over 5 columns */

      // the row data just comes from the font
      // see https://www.nongnu.org/avr-libc/user-manual/pgmspace.html
      // if you want to understand the following line :)
      uint8_t d = pgm_read_byte( &(Font5x7[(str[c]-0x20)*5+i]));

      // the last byte must have nCE=H at the end
      if( c == 3 && i == 4)
	hcms_shiftout( d, 8, 1);
      else
	hcms_shiftout( d, 8, 0);
    }
  }
}
